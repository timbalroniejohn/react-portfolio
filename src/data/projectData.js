const projectData = [
  {
    id: "1",
    title: "Web Portfolio",
    description: "Front-end web portfolio using HTML, CSS and Bootstrap.",
    link: "/portfolio",
    img: "https://res.cloudinary.com/dicnytoq6/image/upload/v1674564077/Web-portfolio/firstProject1_exhhit.png",
  },
  {
    id: "2",
    title: "E-Commerce API",
    description:
      "Back-end API deployed in Render and tested in Postman. MongoDB, Node.js,Express.js, JWT web-token, Javascript are the tools used in this project.",
    link: "/ecommerce",
    img: "https://res.cloudinary.com/dicnytoq6/image/upload/v1674564933/Web-portfolio/postman_nynmlt.jpg",
  },
  {
    id: "3",
    title: "E-Commerce Website",
    description:
      "A fullstack e-commerce website using MongoDB, Express.js, React.js, and Node.js",
    link: "/ecommerceWebsite",
    img: "https://res.cloudinary.com/dicnytoq6/image/upload/v1674602140/Web-portfolio/Screenshot_from_2023-01-25_07-14-02_voofi6.png",
  },
  {
    id: "4",
    title: "Chat-Bot",
    description:
      "A Chat-GPT based chat bot that uses open-ai API. It is capable of answering programming questions and can be a great assistant for programmers.",
    link: "/chatbot",
    img: "https://res.cloudinary.com/dicnytoq6/image/upload/v1679469818/robot_hero_hlhfif.jpg",
  },
];

export default projectData;
