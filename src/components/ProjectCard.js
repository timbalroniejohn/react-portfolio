import { Col, Row } from "react-bootstrap";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import { Link } from "react-router-dom";
import "../App.css";

function ProjectCard({ projectProp }) {
  const { title, description, img, link, id } = projectProp;
  return (
    <Col className="projectCard-col">
      <Card
        style={{ width: "18rem", height: "30rem", borderColor: "teal" }}
        className="card-style"
      >
        <Card.Img className="image-placeholder" variant="top" src={img} />
        <Card.Body>
          <Card.Title>{title}</Card.Title>
          <Card.Text>{description}</Card.Text>
        </Card.Body>
        <Button className="m-1 btn-product" as={Link} to={link}>
          DETAILS
        </Button>
      </Card>
    </Col>
  );
}

export default ProjectCard;
