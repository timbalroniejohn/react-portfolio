import React from 'react';
import 'animate.css';
import '../App.css';

export default function Title() {
  return (
    <div className="title-container">
        <div className="title-component">
            <h1 className="title-name" >Ronie John Timbal</h1>
            <h3 className="subtitle">Web Developer</h3>
        </div>
    </div>
  )
}
