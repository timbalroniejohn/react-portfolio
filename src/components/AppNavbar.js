import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { Link } from 'react-router-dom';
import '../App.css';



function AppNavbar() {
  return (
    <Navbar fixed="top" bg="light" expand="lg">
      <Container>
        <Navbar.Brand className="name" as={Link} to="/profile">Ronie John Timbal</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="ms-auto">

            <Nav.Link className="ms-5 navigation" as={Link} to="/">Home</Nav.Link>
            <Nav.Link className="ms-5 navigation" as={Link} to="/projects">Projects</Nav.Link>
            <Nav.Link className="ms-5 navigation" as={Link} to="/contact">Contact</Nav.Link>
            <Nav.Link className="ms-5 navigation" as={Link} to="/profile">Profile</Nav.Link>
           
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default AppNavbar;