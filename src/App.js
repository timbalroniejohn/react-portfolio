import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import Home from "./pages/Home";
import AppNavbar from "./components/AppNavbar";
import Projects from "./pages/Projects";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Portfolio from "./pages/Portfolio";
import Ecommerce from "./pages/Ecommerce";
import EcommerceWebsite from "./pages/EcommerceWebsite";
import Profile from "./pages/Profile";
import Contact from "./pages/Contact";
import Chatbot from "./pages/Chatbot";

export default function App() {
  return (
    <Router>
      <AppNavbar />
      <Routes>
        <Route exact path="/" element={<Home />} />
        <Route exact path="/projects" element={<Projects />} />
        <Route exact path="/contact" element={<Contact />} />
        <Route exact path="/profile" element={<Profile />} />
        <Route exact path="/portfolio" element={<Portfolio />} />
        <Route exact path="/ecommerce" element={<Ecommerce />} />
        <Route exact path="/ecommerceWebsite" element={<EcommerceWebsite />} />
        <Route exact path="/chatbot" element={<Chatbot />} />
      </Routes>
    </Router>
  );
}
