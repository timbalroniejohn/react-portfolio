import React, { useEffect, useState } from "react";
import "../App.css";
import Button from "react-bootstrap/Button";
import PhoneAndroidOutlinedIcon from "@mui/icons-material/PhoneAndroidOutlined";
import { EmailOutlined, LinkedIn } from "@mui/icons-material";
import { useRef } from "react";
import emailjs from "@emailjs/browser";
import Swal from "sweetalert2";

export default function Contact() {
  const [fullName, setFullName] = useState("");
  const [email, setEmail] = useState("");
  const [message, setMessage] = useState("");
  const [isActive, setIsActive] = useState(false);

  useEffect(() => {
    if (fullName !== "" && email !== "" && message !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [fullName, email, message]);

  const form = useRef();

  const sendEmail = (e) => {
    e.preventDefault();

    emailjs
      .sendForm(
        "service_8jq4vvz",
        "template_fdfdvly",
        form.current,
        "zHO9Mpf0or9lU4O6I"
      )
      .then(
        (result) => {
          console.log(result.text);
          Swal.fire({
            icon: "success",
            title: "Sent",
            text: "Thank you for reaching out!",
            color: "teal",
            confirmButtonColor: "teal",
          });

          setFullName("");
          setEmail("");
          setMessage("");
        },
        (error) => {
          console.log(error.text);
        }
      );
  };

  return (
    <div className="contact-div" id="contact">
      <div>
        <form className="contact-form" ref={form} onSubmit={sendEmail}>
          <h1 className="contact-header">Contact-Me</h1>
          <label className="mb-3">Name</label>
          <input
            className="mb-3"
            style={{ height: "40px", borderRadius: "10px" }}
            type="text"
            name="user_name"
            placeholder="Enter full name"
            value={fullName}
            onChange={(e) => setFullName(e.target.value)}
          />

          <label className="mb-3">Email</label>
          <input
            className="mb-3"
            style={{ height: "40px", borderRadius: "10px" }}
            type="email"
            name="user_email"
            placeholder="Enter email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />

          <label className="mb-3">Message</label>
          <textarea
            name="message"
            style={{ height: "100px", borderRadius: "10px" }}
            placeholder="Type here"
            value={message}
            onChange={(e) => setMessage(e.target.value)}
          />

          {isActive ? (
            <Button className="btn-form" type="submit" value="Send">
              Submit
            </Button>
          ) : (
            <Button
              className="btn-form"
              variant="secondary"
              type="submit"
              value="Send"
              disabled
            >
              Submit
            </Button>
          )}
        </form>
      </div>
      <div className="footer">
        <div className="footer-container">
          <ul className="footer-contact-section">
            <li>
              <a
                href="/profile"
                className="hover-item"
                style={{ textDecoration: "none", color: "white" }}
              >
                <EmailOutlined />
              </a>
            </li>
            <li>
              <a
                href="/profile"
                className="hover-item"
                style={{ textDecoration: "none", color: "white" }}
              >
                <PhoneAndroidOutlinedIcon />
              </a>
            </li>
            <li>
              <a
                href="/profile"
                className="hover-item"
                style={{ textDecoration: "none", color: "white" }}
              >
                <LinkedIn />
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
}
