import "../App.css";
import Carousel from "react-bootstrap/Carousel";
import { Button } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function EcommerceWebsite() {
  return (
    <div className="text-center">
      <Carousel className="carousel-properties" variant="dark">
        <Carousel.Item interval={1000}>
          <img
            className="carousel-image"
            src="https://res.cloudinary.com/dicnytoq6/image/upload/v1679469818/robot_hero_hlhfif.jpg"
            alt="First slide"
          />
        </Carousel.Item>
        <Carousel.Item interval={500}>
          <img
            className="carousel-image"
            src="https://res.cloudinary.com/dicnytoq6/image/upload/v1679470787/Screenshot_from_2023-03-22_15-38-03_e4jq45.png"
            alt="Second slide"
          />
        </Carousel.Item>
        <Carousel.Item>
          <img
            className="carousel-image"
            src="https://res.cloudinary.com/dicnytoq6/image/upload/v1679470787/Screenshot_from_2023-03-22_15-39-32_muygsm.png"
            alt="Third slide"
          />
        </Carousel.Item>
      </Carousel>
      <div className="project-title-div text-center">
        <h1 className="text-center">Chat Bot</h1>
        <p className="m-2 text-justify">
          A Chat-GPT based chat bot that uses open-ai API. It is capable of
          answering programming questions and can be a great assistant for
          programmers.
        </p>
      </div>
      <Button className="btn-portfolio" as={Link} to="/projects">
        Project List
      </Button>
      <Button
        className="btn-portfolio"
        href="https://chatbot-client-v2.vercel.app/"
        target="_blank"
        rel="nnoopener noreferrer"
      >
        Project Link
      </Button>
    </div>
  );
}
