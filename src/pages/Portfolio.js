import '../App.css';
import Carousel from 'react-bootstrap/Carousel';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Portfolio() {
  return (
   <div className="text-center">
        <Carousel className="carousel-properties" variant="dark">
        <Carousel.Item interval={1000}>
            <img
            className="carousel-image"
            src="https://res.cloudinary.com/dicnytoq6/image/upload/v1674564077/Web-portfolio/firstProject1_exhhit.png"
            alt="First slide"
            />
           
        </Carousel.Item>
        <Carousel.Item interval={500}>
            <img
            className="carousel-image"
            src="https://res.cloudinary.com/dicnytoq6/image/upload/v1674564078/Web-portfolio/firstProject2_puiutq.png"
            alt="Second slide"
            />
        </Carousel.Item>
        <Carousel.Item>
            <img
            className="carousel-image"
            src="https://res.cloudinary.com/dicnytoq6/image/upload/v1674612765/Web-portfolio/Screenshot_from_2023-01-25_10-12-17_sc26di.png"
            alt="Third slide"
            />
        </Carousel.Item>
        </Carousel>
        <div className="project-title-div text-center">
            <h1 className="text-center">Web Developer Portfolio</h1>
            <p className="m-2 text-justify">Creating my first project and developer portfolio. Minimalist design was applied in this project. It was developed with the use of HTML, CSS, Bootstrap and Animate on Scroll. The design was inspired with white and black theme with a blue accent. Also, it is mobile responsive.</p>
        </div>
        <Button className="btn-portfolio" as={Link} to="/projects">Project List</Button>
        <Button className="btn-portfolio" href="https://ronie2022.github.io/web-portfolio/" target="_blank" rel="nnoopener noreferrer" >Project Link</Button>
   </div>
  )
}
