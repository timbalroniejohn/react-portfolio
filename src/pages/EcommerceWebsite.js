import '../App.css';
import Carousel from 'react-bootstrap/Carousel';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function EcommerceWebsite() {
  return (
   <div className="text-center">
        <Carousel className="carousel-properties" variant="dark">
        <Carousel.Item interval={1000}>
            <img
            className="carousel-image"
            src="https://res.cloudinary.com/dicnytoq6/image/upload/v1674639900/Screenshot_from_2023-01-25_17-44-37_eckvlv.png"
            alt="First slide"
            />
           
        </Carousel.Item>
        <Carousel.Item interval={500}>
            <img
            className="carousel-image"
            src="https://res.cloudinary.com/dicnytoq6/image/upload/v1674639850/Screenshot_from_2023-01-25_17-40-03_kjvsxt.png"
            alt="Second slide"
            />
        </Carousel.Item>
        <Carousel.Item>
            <img
            className="carousel-image"
            src="https://res.cloudinary.com/dicnytoq6/image/upload/v1674639851/Screenshot_from_2023-01-25_17-43-43_drv28x.png"
            alt="Third slide"
            />
        </Carousel.Item>
        </Carousel>
        <div className="project-title-div text-center">
            <h1 className="text-center">E-commerce Website</h1>
            <p className="m-2 text-justify">Full-stack (MERN Stack) website. The tools used to develop this website are MongoDB, Express.js, React.js, and Node.js. It uses react framework to make it dynamic and Bootstrap for its design.</p>
        </div>
        <Button className="btn-portfolio" as={Link} to="/projects">Project List</Button>
        <Button className="btn-portfolio" href="https://ecommerce-react-app-ruddy.vercel.app/" target="_blank" rel="nnoopener noreferrer" >Project Link</Button>
   </div>
  )
}
