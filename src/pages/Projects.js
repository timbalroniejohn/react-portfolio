import React from "react";
import projectData from "../data/projectData";
import "../App.css";
import ProjectCard from "../components/ProjectCard";
import { Row } from "react-bootstrap";

export default function Projects() {
  const projects = projectData.map((project) => {
    return <ProjectCard key={project.id} projectProp={project} />;
  });

  return (
    <div className="projects-div" id="projects">
      <h1 className="projects-header">Projects</h1>
      <Row className="project-row">{projects}</Row>
      <div className="toolbox">
        <div>
          <img
            className="toolbox-image"
            src="https://res.cloudinary.com/dicnytoq6/image/upload/v1674708753/visual-studio-code-icon_a0x3jq.svg"
            width="50px"
            height="50px"
            alt="vs-code"
          />
        </div>
        <div>
          <img
            className="toolbox-image"
            src="https://res.cloudinary.com/dicnytoq6/image/upload/v1674708753/react-js-icon_aue9wy.svg"
            width="50px"
            height="50px"
            alt="react"
          />
        </div>
        <div>
          <img
            className="toolbox-image"
            src="https://res.cloudinary.com/dicnytoq6/image/upload/v1674708753/postman-icon_xxd8jt.svg"
            width="50px"
            height="50px"
            alt="postman"
          />
        </div>
        <div>
          <img
            className="toolbox-image"
            src="https://res.cloudinary.com/dicnytoq6/image/upload/v1674708753/nodejs_ndjtf4.svg"
            width="50px"
            height="50px"
            alt="nodejs"
          />
        </div>
        <div>
          <img
            className="toolbox-image"
            src="https://res.cloudinary.com/dicnytoq6/image/upload/v1674708752/mongodb_vnz4xa.svg"
            width="50px"
            height="50px"
            alt="mongoDB"
          />
        </div>
        <div>
          <img
            className="toolbox-image"
            src="https://res.cloudinary.com/dicnytoq6/image/upload/v1674708752/git-icon_xxpokn.svg"
            width="50px"
            height="50px"
            alt="git"
          />
        </div>
        <div>
          <img
            className="toolbox-image"
            src="https://res.cloudinary.com/dicnytoq6/image/upload/v1674708752/bootstrap-5-logo-icon_shqjfa.svg"
            width="50px"
            height="50px"
            alt="bootstrap"
          />
        </div>
        <div>
          <img
            className="toolbox-image"
            src="https://res.cloudinary.com/dicnytoq6/image/upload/v1674708752/html-icon_egeqqh.svg"
            width="50px"
            height="50px"
            alt="html"
          />
        </div>
        <div>
          <img
            className="toolbox-image"
            src="https://res.cloudinary.com/dicnytoq6/image/upload/v1674708751/express-js-icon_yyjvvq.svg"
            width="50px"
            height="50px"
            alt="express-js"
          />
        </div>
      </div>
    </div>
  );
}
