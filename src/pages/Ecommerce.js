import '../App.css';
import Carousel from 'react-bootstrap/Carousel';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Ecommerce() {
  return (
   <div className="text-center">
        <Carousel className="carousel-properties" variant="dark">
        <Carousel.Item interval={1000}>
            <img
            className="carousel-image"
            src="https://res.cloudinary.com/dicnytoq6/image/upload/v1674639029/code_ryhsdf.png"
            alt="First slide"
            />
           
        </Carousel.Item>
        <Carousel.Item interval={500}>
            <img
            className="carousel-image"
            src="https://res.cloudinary.com/dicnytoq6/image/upload/v1674639030/postman_yek2iy.jpg"
            alt="Second slide"
            />
        </Carousel.Item>
        <Carousel.Item>
            <img
            className="carousel-image"
            src="https://res.cloudinary.com/dicnytoq6/image/upload/v1674639030/render_dgzd1w.jpg"
            alt="Third slide"
            />
        </Carousel.Item>
        </Carousel>
        <div className="project-title-div text-center">
            <h1 className="text-center">E-commerce API</h1>
            <p className="m-2 text-justify">Back-end development is about creating, coding, and improving the server, server-side applications, and databases that, when combined with front-end codes, helps create a functional, seamless experience for the end-user. In this project, I created an API that is connected to MongoDB and simulated the request and response through the use of PostMan application. It's functions is quite similar to Shopee and Lazada application.</p>
        </div>
        <Button className="btn-portfolio" as={Link} to="/projects">Project List</Button>
        <Button className="btn-portfolio" href="https://gitlab.com/b224-timbal/ecommerce-api/ecommerce" target="_blank" rel="nnoopener noreferrer" >Project Link</Button>
   </div>
  )
}
