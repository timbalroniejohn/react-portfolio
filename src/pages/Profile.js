import React from "react";
import Card from "react-bootstrap/Card";
import ListGroup from "react-bootstrap/ListGroup";
import "../App.css";

export default function Profile() {
  return (
    <div className="profile-page" id="profile">
      <div className="profile-card">
        <Card className="profile-card-style">
          <Card.Img
            variant="top"
            className="cover-photo"
            src="https://res.cloudinary.com/dicnytoq6/image/upload/v1674009610/pexels-eberhard-grossgasteiger-1287145_u0edjb.jpg"
          />
          <Card.Img
            variant="top"
            className="profile-photo"
            src="https://res.cloudinary.com/dicnytoq6/image/upload/v1674701017/profile_ybzlil.jpg"
          />
          <Card.Body>
            <Card.Title className="card-title">Ronie John Timbal</Card.Title>
            <Card.Text>
              Web developer who has the passion in designing and creating user
              friendly website. Knowledgeable in frontend, backend and fullstack
              web development. Cares deeply about user experience.
            </Card.Text>
          </Card.Body>
          <ListGroup className="list-group-flush pt-3">
            <ListGroup.Item>
              <b>Address:</b> Boyo-an Sur, Candijay, Bohol, Philippines, 6312
            </ListGroup.Item>
            <ListGroup.Item>
              <b>Email:</b> timbalroniejohn@gmail.com
            </ListGroup.Item>
            <ListGroup.Item>
              <b>Mobile:</b> +63 916 873 1296
            </ListGroup.Item>
            <ListGroup.Item>
              <b>LinkedIn:</b>{" "}
              <a
                href="https://www.linkedin.com/in/ronie-john-timbal-884b68229/"
                target="_blank"
                rel="nnoopener noreferrer"
              >
                https://www.linkedin.com/in/ronie-john-timbal-884b68229/
              </a>
            </ListGroup.Item>
            <ListGroup.Item>
              <b>Resume:</b>{" "}
              <a
                href="https://drive.google.com/file/d/1cphRdH0KM0rJ2KX8ZIzwDcD6rg_M93zW/view?usp=share_link"
                target="_blank"
                rel="nnoopener noreferrer"
              >
                View
              </a>
            </ListGroup.Item>
          </ListGroup>
        </Card>
      </div>
    </div>
  );
}
