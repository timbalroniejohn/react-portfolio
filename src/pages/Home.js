import React from "react";
import Title from "../components/Title";
import Contact from "./Contact";
import Projects from "./Projects";

export default function Home() {
  return (
    <div>
      <Title />
      <Projects />
      <Contact />
    </div>
  );
}
